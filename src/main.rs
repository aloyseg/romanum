pub mod roman;

#[allow(unused_imports)]
use roman::{generate, ToError};

#[test]
fn test_generate() {
    let err0 = generate(0).expect_err("Not a ToError");
    println!("Expected Error for 0   : \"{}\"", err0);
    assert_eq!(err0, ToError::TooSmall);

    let err4k = generate(4_000).expect_err("Not a ToError");
    println!("Expected Error for 4000: \"{}\"", err4k);
    assert_eq!(err4k, ToError::TooBig);

    fn assert_ator(a: u16, r: &str) {
        assert_eq!(generate(a).ok(), Some(String::from(r)));
    }

    // 1-3
    assert_ator(1, "I");
    assert_ator(3, "III");
    assert_ator(10, "X");
    assert_ator(30, "XXX");
    assert_ator(100, "C");
    assert_ator(300, "CCC");
    assert_ator(1000, "M");
    assert_ator(3000, "MMM");
    assert_ator(2222, "MMCCXXII");

    // 4
    assert_ator(4, "IV");
    assert_ator(40, "XL");
    assert_ator(400, "CD");

    // 5
    assert_ator(5, "V");
    assert_ator(50, "L");
    assert_ator(500, "D");

    // 6-8
    assert_ator(6, "VI");
    assert_ator(8, "VIII");
    assert_ator(60, "LX");
    assert_ator(80, "LXXX");
    assert_ator(600, "DC");
    assert_ator(800, "DCCC");
    assert_ator(777, "DCCLXXVII");

    // 9
    assert_ator(9, "IX");
    assert_ator(90, "XC");
    assert_ator(900, "CM");

    // longest
    assert_ator(3888, "MMMDCCCLXXXVIII");

    // max
    assert_ator(3999, "MMMCMXCIX");
}

fn main() {
    println!("ROMAn NUMbers...");
}
