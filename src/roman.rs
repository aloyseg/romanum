use std::fmt;

#[derive(Debug, PartialEq)]
pub enum ToError {
    NaN,
    TooSmall,
    TooBig,
}

// TIP: use https://docs.rs/strum/latest/strum/
impl fmt::Display for ToError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::NaN => "Not A Number",
                Self::TooSmall => "Number less than 1",
                Self::TooBig => "Number greater than 3999",
            }
        )
    }
}

#[derive(Debug)]
pub enum FromError {
    Empty, // = "empty Roman",
}

/// structure holding the rest="what needs to be converted" and rn="partial roman number"
struct PartialRoman {
    rest: u16,
    rn: String,
}

impl PartialRoman {
    /// nothing left to be converted
    fn is_done(self: &Self) -> bool {
        self.rest == 0
    }
    /// builds 1-3 *i*s if rest >= factor else nothing
    fn i3(self: Self, fact: u16, i: &str) -> PartialRoman {
        if self.rest >= fact {
            let de = self.rest.div_euclid(fact);
            return PartialRoman {
                rest: self.rest - de * fact,
                rn: i.repeat(de.into()),
            };
        } else {
            return self;
        }
    }

    /// builds ivx combinations 1-9 * factor if rest >= factor else nothing
    fn ivx(self: Self, fact: u16, i: &str, v: &str, x: &str) -> PartialRoman {
        // needs building ?
        if self.rest >= fact {
            // trunacte to fact
            let de = self.rest.div_euclid(fact);

            return PartialRoman {
                rest: self.rest - de * fact,
                // ? factor out "self.rn +" ?
                rn: match de {
                    // i, ii, iii
                    n if n < 4 => self.rn + &i.repeat(de.into()),
                    // iv
                    4 => self.rn + &i + &v,
                    // v
                    5 => self.rn + &v,
                    // ix
                    9 => self.rn + &i + &x,
                    // vi, vii, viii
                    n if n > 5 => self.rn + &v + &i.repeat((de - 5).into()),
                    // exhaustive check - make compiler happy
                    _ => panic!("never happnes ;-)"),
                },
            };
        } else {
            return self;
        }
    }
}

/// generates Roman from Arabic - n in 1..3999 => {I,V,X,L,C,D,M}*...
pub fn generate(n: u16) -> Result<String, ToError> {
    if n < 1 {
        Err(ToError::TooSmall)
    } else if n > 3999 {
        Err(ToError::TooBig)
    } else {
        // initialize
        let pr = PartialRoman {
            rest: n,
            rn: String::from(""),
        };

        // 1000 -> 3000
        let pr = pr.i3(1000, "M");
        // 100 -> 900
        let pr = pr.ivx(100, "C", "D", "M");
        // 10 -> 90
        let pr = pr.ivx(10, "X", "L", "C");
        // 1 -> 9
        let pr = pr.ivx(1, "I", "V", "X");

        if pr.is_done() {
            Ok(pr.rn)
        } else {
            Err(ToError::NaN)
        }
    }
}
